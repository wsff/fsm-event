package org.wsff.tools.state.core;

import lombok.Getter;

import org.wsff.tools.state.event.BaseEvent;

/**
 * state event model
 *
 * @author ryan
 * @version Id: StateEvent.java, v 0.1 2022-03-24 16:38 ryan Exp $$
 */
@Getter
public abstract class StateEvent extends BaseEvent {
    private static final long  serialVersionUID = 176835855121204410L;

    /** 事件上下文 */
    private final StateContext context;

    /**
     * Constructs a prototypical Event.
     *
     * @param    source    The object on which the Event initially occurred.
     * @param    context state context                    
     * @param    id    The Event id.
     * @exception IllegalArgumentException  if source is null.
     */
    public StateEvent(Object source, StateContext context, String id) {
        super(source, id);
        this.context = context;
    }

}
