package org.wsff.tools.state.core;

/**
 * state manager
 *
 * @author ryan
 * @version Id: StateManager.java, v 0.1 2022-03-24 15:46 ryan Exp $$
 */
public interface StateManager {

    /**
     * set state
     *
     * @param context state context
     * @param to to state
     */
    void setState(StateContext context, State to);
}
