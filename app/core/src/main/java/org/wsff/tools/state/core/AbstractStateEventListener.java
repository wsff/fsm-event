package org.wsff.tools.state.core;

import javax.annotation.Resource;

/**
 * abstract state event listener
 *
 * @author ryan
 * @version Id: AbstractStateEventListener.java, v 0.1 2022-03-25 16:56 ryan Exp $$
 */
public abstract class AbstractStateEventListener<E extends StateEvent> implements StateEventListener<E> {

    @Resource
    protected StateManager stateManager;

    /**
     * Gets the value of stateManager.
     *
     * @return the value of stateManager
     */
    public StateManager getStateManager() {
        return stateManager;
    }

    /**
     * Sets the stateManager. *
     * <p>You can use getStateManager() to get the value of stateManager</p >
     * @param stateManager stateManager
     */
    public void setStateManager(StateManager stateManager) {
        this.stateManager = stateManager;
    }
}
