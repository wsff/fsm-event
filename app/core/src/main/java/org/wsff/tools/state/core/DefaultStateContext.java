package org.wsff.tools.state.core;

import lombok.Getter;

/**
 * DefaultStateContext
 * @author ryan
 * @version Id: DefaultStateContext.java, v 0.1 2022-03-28 17:40 ryan Exp $$
 */
@Getter
public class DefaultStateContext implements StateContext {

    private final State from;

    public DefaultStateContext(State from) {
        this.from = from;
    }

    /**
     * 变更前状态
     *
     * @return 状态
     */
    @Override
    public State from() {
        return from;
    }
}
