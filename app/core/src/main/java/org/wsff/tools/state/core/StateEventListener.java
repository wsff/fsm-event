package org.wsff.tools.state.core;

import org.wsff.tools.state.event.BaseEventListener;

/**
 * state event listener
 *
 * @author ryan
 * @version Id: StateEventListener.java, v 0.1 2022-03-24 16:38 ryan Exp $$
 */
public interface StateEventListener<E extends StateEvent> extends BaseEventListener<E> {

    /**
     * get state event code
     *
     * @param event state event
     * @return event code
     */
    default String stateEventCode(E event) {
        return event.getContext().from().id();
    }
}
