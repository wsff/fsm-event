package org.wsff.tools.state.core;

import java.util.function.Function;

/**
 * state transfer
 *
 * @author ryan
 * @version Id: StateTransfer.java, v 0.1 2022-03-25 11:38 ryan Exp $$
 */
public interface StateTransfer {

    /**
     * get original state
     *
     * @return original state
     */
    String from();

    /**
     * get changed state
     *
     * @return state
     */
    String to();

    /**
     * need sync handle
     *
     * @return default is false
     */
    default boolean sync(StateContext context) {
        return false;
    }

    /**
     * get state event object
     *
     * @param context state context
     * @return state event
     */
    StateEvent event(StateContext context);

    static <R extends StateEvent, T extends StateContext> StateTransfer of(String from, String to, Function<T, R> function) {
        return new StateTransfer() {
            @Override
            public String from() {
                return from;
            }

            @Override
            public String to() {
                return to;
            }

            @Override
            public StateEvent event(StateContext context) {
                return function.apply((T) context);
            }
        };
    }
}
