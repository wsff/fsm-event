package org.wsff.tools.state.core;

/**
 * state transfer provider
 *
 * @author ryan
 * @version Id: StateTransferProvider.java, v 0.1 2022-03-25 11:41 ryan Exp $$
 */
public interface StateTransferProvider extends LifeCycle {

    /**
     * get state transfer
     *
     * @param from form state
     * @param to to state
     * @return state transfer
     */
    StateTransfer get(State from, State to);
}
