package org.wsff.tools.state.core;

/**
 * state context
 *
 * @author ryan
 * @version Id: StateContext.java, v 0.1 2022-03-24 15:58 ryan Exp $$
 */
public interface StateContext {

    /**
     * 变更前状态
     *
     * @return 状态
     */
    State from();
}
