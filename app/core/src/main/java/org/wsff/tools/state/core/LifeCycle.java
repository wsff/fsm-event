package org.wsff.tools.state.core;

/**
 * LifeCycle interface
 *
 * @author ryan
 * @version Id: LifeCycle.java, v 0.1 2022-03-25 17:00 ryan Exp $$
 */
public interface LifeCycle {

    /**
     * init when the container is started
     */
    void init();

    /**
     * destroy when the container is stopped
     */
    void destroy();

}
