package org.wsff.tools.state.core;

/**
 * 状态
 *
 * @author ryan
 * @version Id: State.java, v 0.1 2022-03-24 15:59 ryan Exp $$
 */
public interface State {

    /**
     * 状态机唯一id
     *
     * @return 唯一id
     */
    String id();

    /**
     * anonymous state
     *
     * @param id state id
     * @return state
     */
    static State of(String id) {
        return () -> id;
    }

}
