package org.wsff.tools.state.core.spring;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.wsff.tools.state.core.AbstractStateManager;
import org.wsff.tools.state.core.StateEventListener;

/**
 * AbstractSpringStateManager
 * @author ryan
 * @version Id: AbstractSpringStateManager.java, v 0.1 2022-03-28 15:21 ryan Exp $$
 */
public abstract class AbstractSpringStateManager extends AbstractStateManager {

    /**
     * get all state event listener
     *
     * @return state listener
     */
    @Override
    protected List<StateEventListener> getAllStateEventListeners() {
        ApplicationContext context = SpringApplicationContextHolder.getContext();
        Map<String, StateEventListener> listenersMap = context.getBeansOfType(StateEventListener.class, false, true);
        return new ArrayList<>(listenersMap.values());
    }
}
