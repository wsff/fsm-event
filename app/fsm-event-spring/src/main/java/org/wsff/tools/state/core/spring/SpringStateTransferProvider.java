package org.wsff.tools.state.core.spring;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.wsff.tools.state.core.AbstractStateTransferProvider;
import org.wsff.tools.state.core.StateTransfer;

/**
 * SpringStateTransferProvider
 * @author ryan
 * @version Id: SpringStateTransferProvider.java, v 0.1 2022-03-28 15:23 ryan Exp $$
 */
public class SpringStateTransferProvider extends AbstractStateTransferProvider {

    /**
     * get all state transfers
     *
     * @return state transfers
     */
    @Override
    protected List<StateTransfer> getAllStateTransfers() {
        ApplicationContext context = SpringApplicationContextHolder.getContext();
        Map<String, StateTransfer> transfers = context.getBeansOfType(StateTransfer.class, false, true);
        return new ArrayList<>(transfers.values());
    }

}
