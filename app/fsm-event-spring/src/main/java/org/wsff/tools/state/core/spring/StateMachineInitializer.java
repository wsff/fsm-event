package org.wsff.tools.state.core.spring;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.wsff.tools.state.core.LifeCycle;

/**
 * StateMachineInitializer
 * @author ryan
 * @version Id: StateMachineInitializer.java, v 0.1 2022-03-28 15:28 ryan Exp $$
 */
@Service
public class StateMachineInitializer implements ApplicationContextAware {

    /**
     * Set the ApplicationContext that this object runs in.
     * @see BeanInitializationException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringApplicationContextHolder.setContext(applicationContext);
        Map<String, LifeCycle> beans = applicationContext.getBeansOfType(LifeCycle.class, false, true);
        for (Map.Entry<String, LifeCycle> entry : beans.entrySet()) {
            entry.getValue().init();
        }
    }
}
