package org.wsff.tools.state.core.spring;

import org.springframework.context.ApplicationContext;

/**
 * SpringApplicationContextHolder
 * @author ryan
 * @version Id: SpringApplicationContextHolder.java, v 0.1 2022-03-28 15:31 ryan Exp $$
 */
public class SpringApplicationContextHolder {

    private static ApplicationContext context;

    public static ApplicationContext getContext() {
        return context;
    }

    public static void setContext(ApplicationContext applicationContext) {
        context = applicationContext;
    }
}
