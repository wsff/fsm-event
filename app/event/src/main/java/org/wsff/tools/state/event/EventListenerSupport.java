
package org.wsff.tools.state.event;

import java.util.List;

/**
 * EventListenerSupport
 *
 * @author ryan
 * @version Id: EventListenerSupport.java, v 0.1 2022-03-25 17:07 ryan Exp $$
 */
public interface EventListenerSupport {

    /**
     * registered listener
     *
     * @param listener event listener
     */
    void addListener(BaseEventListener<BaseEvent> listener);

    /**
     * remove registered listener
     *
     * @param listener event listener
     */
    void removeListener(BaseEventListener<BaseEvent> listener);

    /**
     * remove all registered listener
     */
    void removeAllListeners();

    /**
     * get all event listener
     *
     * @return all listener
     */
    List<BaseEventListener<BaseEvent>> getAll();

    /**
     * get all support event listener by event
     *
     * @param event event
     * @return all listener
     */
    List<BaseEventListener<BaseEvent>> getAllSupportListeners(BaseEvent event);
}
