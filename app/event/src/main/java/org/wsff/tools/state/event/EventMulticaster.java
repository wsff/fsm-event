
package org.wsff.tools.state.event;

/**
 * event caster
 *
 * @author ryan
 * @version Id: EventMulticaster.java, v 0.1 2022-03-25 14:46 ryan Exp $$
 */
public interface EventMulticaster {

    /**
     * multicast event
     *
     * @param event event
     * @param sync sync or async
     */
    void multicastEvent(BaseEvent event, boolean sync);
}
