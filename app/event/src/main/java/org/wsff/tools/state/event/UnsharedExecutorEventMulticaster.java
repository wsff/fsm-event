package org.wsff.tools.state.event;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.Executor;

import org.apache.commons.lang3.StringUtils;

/**
 * unshared executor event multicaster
 *
 * @author ryan
 * @version Id: UnsharedExecutorEventMulticaster.java, v 0.1 2022-03-28 09:52 ryan Exp $$
 */
@Slf4j
public class UnsharedExecutorEventMulticaster extends AbstractAsyncEventMulticaster {

    /**
     * Asynchronous executor group by event tag
     */
    @Setter
    private Map<String, Executor> taskExecutors;

    /**
     * Gets the value of taskExecutor.
     *
     * @return the value of taskExecutor
     */
    @Override
    protected Executor getTaskExecutor(BaseEvent event) {
        if (taskExecutors == null || taskExecutors.isEmpty()) {
            if (StringUtils.equals(BaseEvent.DEFAULT_TAG, event.tag())) {
                Executor taskExecutor = AsyncExecutorHolder.getDefaultExecutor();
                taskExecutors.put(BaseEvent.DEFAULT_TAG, taskExecutor);
            }
        }
        return taskExecutors.get(event.tag());
    }
}
