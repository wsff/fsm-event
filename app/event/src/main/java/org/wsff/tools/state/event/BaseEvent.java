package org.wsff.tools.state.event;

import lombok.Getter;

import java.util.EventObject;

/**
 * base event model
 * 
 * @author ryan
 * @version Id: BaseEvent.java, v 0.1 2022-03-25 13:54 ryan Exp $$
 */
@Getter
public class BaseEvent extends EventObject {

    private static final long   serialVersionUID = -2266240783898593523L;

    public static final String DEFAULT_TAG      = "_$default_tag";

    /** 事件id */
    private final String      id;

    /** 事件产生时间 */
    private final long        timestamp;

    /**
     * event code ,different tag will be used different async Executor
     * 
     * @return code
     */
    protected String tag() {
        return DEFAULT_TAG;
    }

    /**
     * Constructs a prototypical Event.
     *
     * @param    source    The object on which the Event initially occurred.
     * @param    id    The Event id.
     * @exception IllegalArgumentException  if source is null.
     */
    public BaseEvent(Object source, String id) {
        super(source);
        this.id = id;
        this.timestamp = System.currentTimeMillis();
    }
}
