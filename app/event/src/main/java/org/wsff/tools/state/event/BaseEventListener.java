package org.wsff.tools.state.event;

import java.util.EventListener;

/**
 * base event listener
 *
 * @author ryan
 * @version Id: BaseEventListener.java, v 0.1 2022-03-25 13:56 ryan Exp $$
 */
public interface BaseEventListener<E extends BaseEvent> extends EventListener {

    /**
     * event handler
     *
     * @param event event
     */
    void onEvent(E event);

    /**
     * is support event
     *
     * @param event event
     * @return support
     */
    boolean isSupportEvent(BaseEvent event);
}
