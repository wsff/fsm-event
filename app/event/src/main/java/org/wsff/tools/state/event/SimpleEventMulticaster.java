package org.wsff.tools.state.event;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executor;

/**
 * simple event multicaster
 *
 * @author ryan
 * @version Id: SimpleEventMulticaster.java, v 0.1 2022-03-28 09:52 ryan Exp $$
 */
@Slf4j
public class SimpleEventMulticaster extends AbstractAsyncEventMulticaster {

    /**
     * Asynchronous executor 
     */
    @Setter
    private Executor taskExecutor;

    /**
     * getTaskExecutor
     *
     * @param event event
     * @return Executor
     */
    @Override
    protected Executor getTaskExecutor(BaseEvent event) {
        if (taskExecutor == null) {
            taskExecutor = AsyncExecutorHolder.getDefaultExecutor();
        }
        return taskExecutor;
    }

}
