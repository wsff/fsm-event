
package org.wsff.tools.state.event;

/**
 * SyncEventMulticaster
 *
 * @author ryan
 * @version Id: SyncEventMulticaster.java, v 0.1 2022-03-28 13:38 ryan Exp $$
 */
public interface SyncEventMulticaster extends EventMulticaster {

    /**
     * multicast event
     *
     * @param event event
     */
    default void multicastEvent(BaseEvent event){
        multicastEvent(event,true);
    }
}
