package org.wsff.tools.state.event;

import java.util.Map;
import java.util.concurrent.Executor;

/**
 * Events
 * @author ryan
 * @version Id: Events.java, v 0.1 2022-03-28 15:11 ryan Exp $$
 */
public class Events {

    /**
     * simple event multicaster
     * 
     * @return EventMulticaster
     */
    public EventMulticaster simpleEventMulticaster() {
        return new SimpleEventMulticaster();
    }

    /**
     * event multicaster
     *
     * @param taskExecutor task executor
     * @return EventMulticaster
     */
    public EventMulticaster SimpleEventMulticaster(Executor taskExecutor) {
        SimpleEventMulticaster eventMulticaster = new SimpleEventMulticaster();
        eventMulticaster.setTaskExecutor(taskExecutor);
        return eventMulticaster;
    }

    /**
     * event multicaster
     *
     * @param taskExecutors task executor group
     * @return EventMulticaster
     */
    public EventMulticaster eventMulticaster(Map<String, Executor> taskExecutors) {
        UnsharedExecutorEventMulticaster eventMulticaster = new UnsharedExecutorEventMulticaster();
        eventMulticaster.setTaskExecutors(taskExecutors);
        return eventMulticaster;
    }
}
