package org.wsff.tools.state.demo;

/**
 * OrderService
 * @author ryan
 * @version Id: OrderService.java, v 0.1 2022-03-28 18:50 ryan Exp $$
 */
public interface OrderService {

    OrderVO create(String id);

    void cancel(OrderVO order);

    void pay(OrderVO order);

    void booked(OrderVO order);
}
