package org.wsff.tools.state.demo.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.wsff.tools.state.core.AbstractStateTransferProvider;
import org.wsff.tools.state.core.StateTransfer;
import org.wsff.tools.state.demo.OrderState;
import org.wsff.tools.state.demo.event.BookedEvent;
import org.wsff.tools.state.demo.event.BookingEvent;
import org.wsff.tools.state.demo.event.CanceledEvent;
import org.wsff.tools.state.demo.event.PaidEvent;
import org.wsff.tools.state.demo.event.PayingEvent;

/**
 * OrderStateTransferProvider
 * @author ryan
 * @version Id: OrderStateTransferProvider.java, v 0.1 2022-03-28 18:21 ryan Exp $$
 */
@Service
public class OrderStateTransferProvider extends AbstractStateTransferProvider {

    /**
     * get all state transfers
     *
     * @return state transfers
     */
    @Override
    protected List<StateTransfer> getAllStateTransfers() {
        List<StateTransfer> transfers = new ArrayList<>();
        transfers.add(StateTransfer.of(OrderState.DRAFT.name(), OrderState.PAYING.name(), (context) -> new PayingEvent(this, context)));
        transfers.add(StateTransfer.of(OrderState.PAYING.name(), OrderState.CANCELED.name(), (context) -> new CanceledEvent(this, context)));
        transfers.add(StateTransfer.of(OrderState.PAYING.name(), OrderState.PAID.name(), (context) -> new PaidEvent(this, context)));
        transfers.add(StateTransfer.of(OrderState.PAID.name(), OrderState.BOOKING.name(), (context) -> new BookingEvent(this, context)));
        transfers.add(StateTransfer.of(OrderState.BOOKING.name(), OrderState.BOOKED.name(), (context) -> new BookedEvent(this, context)));
        return transfers;
    }
}
