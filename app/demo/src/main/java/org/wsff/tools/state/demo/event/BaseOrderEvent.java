package org.wsff.tools.state.demo.event;

import org.wsff.tools.state.core.StateContext;
import org.wsff.tools.state.core.StateEvent;

/**
 * BaseOrderEvent
 * @author ryan
 * @version Id: BaseOrderEvent.java, v 0.1 2022-03-28 18:00 ryan Exp $$
 */
public abstract class BaseOrderEvent extends StateEvent {

    private static final long serialVersionUID = 4926173229777598381L;

    public BaseOrderEvent(Object source, StateContext context) {
        super(source, context, String.valueOf(System.currentTimeMillis()));
    }

    /**
     * event code ,different tag will be used different async Executor
     *
     * @return code
     */
    @Override
    protected String tag() {
        return "ORDER";
    }
}
