package org.wsff.tools.state.demo.event;

import org.wsff.tools.state.core.StateContext;

/**
 * BookedEvent
 * @author ryan
 * @version Id: BookedEvent.java, v 0.1 2022-03-28 18:00 ryan Exp $$
 */
public class BookedEvent extends BaseOrderEvent {

    private static final long serialVersionUID = 4926173229777598381L;

    public BookedEvent(Object source, StateContext context) {
        super(source, context);
    }
}
