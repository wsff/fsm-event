package org.wsff.tools.state.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.wsff.tools.state.event.UnsharedExecutorEventMulticaster;

/**
 * OrderConfig
 * @author ryan
 * @version Id: OrderConfig.java, v 0.1 2022-03-28 21:51 ryan Exp $$
 */
@Configuration
public class OrderConfig {

    @Bean("orderEventMulticaster")
    public UnsharedExecutorEventMulticaster orderEventMulticaster() {
        UnsharedExecutorEventMulticaster eventMulticaster = new UnsharedExecutorEventMulticaster();
        eventMulticaster.setTaskExecutors(taskExecutors());
        return eventMulticaster;
    }

    public Map<String, Executor> taskExecutors() {
        Map<String, Executor> taskExecutors = new HashMap<>();
        taskExecutors.put("ORDER",orderStateExecutor());
        return taskExecutors;
    }

    @Bean("orderStateExecutor")
    public Executor orderStateExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(8);
        executor.setQueueCapacity(32);
        executor.setKeepAliveSeconds(300);
        executor.setThreadNamePrefix("order-state-event");
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }
}
