package org.wsff.tools.state.demo.listener;

import org.springframework.stereotype.Service;
import org.wsff.tools.state.core.AbstractStateEventListener;
import org.wsff.tools.state.core.State;
import org.wsff.tools.state.demo.OrderState;
import org.wsff.tools.state.demo.OrderVO;
import org.wsff.tools.state.demo.event.PaidEvent;
import org.wsff.tools.state.demo.impl.OrderStateContext;
import org.wsff.tools.state.event.BaseEvent;

/**
 * OrderPaidListener
 * @author ryan
 * @version Id: OrderPaidListener.java, v 0.1 2022-03-28 19:00 ryan Exp $$
 */
@Service
public class OrderPaidListener extends AbstractStateEventListener<PaidEvent> {


    /**
     * event handler
     *
     * @param event event
     */
    @Override
    public void onEvent(PaidEvent event) {
        OrderVO order = ((OrderStateContext) event.getContext()).getOrder();
        System.out.println("Subscribe order " + order.getId() + " PaidEvent");

        stateManager.setState(OrderStateContext.of(order), State.of(OrderState.BOOKING.name()));
    }

    /**
     * is support event
     *
     * @param event event
     * @return support
     */
    @Override
    public boolean isSupportEvent(BaseEvent event) {
        return event instanceof PaidEvent;
    }
}
