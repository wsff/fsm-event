package org.wsff.tools.state.demo.listener;

import org.springframework.stereotype.Service;
import org.wsff.tools.state.core.AbstractStateEventListener;
import org.wsff.tools.state.demo.OrderVO;
import org.wsff.tools.state.demo.event.BookingEvent;
import org.wsff.tools.state.demo.impl.OrderStateContext;
import org.wsff.tools.state.event.BaseEvent;

/**
 * OrderBookingListener
 * @author ryan
 * @version Id: OrderBookingListener.java, v 0.1 2022-03-28 19:00 ryan Exp $$
 */
@Service
public class OrderBookingListener extends AbstractStateEventListener<BookingEvent> {

    /**
     * event handler
     *
     * @param event event
     */
    @Override
    public void onEvent(BookingEvent event) {
        OrderVO order = ((OrderStateContext) event.getContext()).getOrder();
        System.out.println("Subscribe order " + order.getId() + " BookingEvent");
    }

    /**
     * is support event
     *
     * @param event event
     * @return support
     */
    @Override
    public boolean isSupportEvent(BaseEvent event) {
        return event instanceof BookingEvent;
    }
}
