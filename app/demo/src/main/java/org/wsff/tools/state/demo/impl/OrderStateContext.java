package org.wsff.tools.state.demo.impl;

import lombok.Data;

import org.wsff.tools.state.core.State;
import org.wsff.tools.state.core.StateContext;
import org.wsff.tools.state.demo.OrderVO;

/**
 * demo order state context
 *
 * @author ryan
 * @version Id: OrderStateContext.java, v 0.1 2022-03-28 17:49 ryan Exp $$
 */
@Data
public class OrderStateContext implements StateContext {

    private OrderVO order;

    public OrderStateContext(OrderVO order) {
        this.order = order;
    }

    public static StateContext of(OrderVO order) {
        return new OrderStateContext(order);
    }

    /**
     * 变更前状态
     *
     * @return 状态
     */
    @Override
    public State from() {
        return State.of(order.getState().name());
    }
}
