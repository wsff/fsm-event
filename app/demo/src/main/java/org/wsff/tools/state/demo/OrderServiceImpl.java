package org.wsff.tools.state.demo;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.wsff.tools.state.core.State;
import org.wsff.tools.state.core.StateManager;
import org.wsff.tools.state.demo.impl.OrderStateContext;

/**
 * OrderServiceImpl
 * @author ryan
 * @version Id: OrderServiceImpl.java, v 0.1 2022-03-28 18:52 ryan Exp $$
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private StateManager stateManager;

    @Override
    public OrderVO create(String id) {
        OrderVO order = new OrderVO(id);
        stateManager.setState(OrderStateContext.of(order), State.of(OrderState.PAYING.name()));
        return order;
    }

    @Override
    public void cancel(OrderVO order) {
        stateManager.setState(OrderStateContext.of(order), State.of(OrderState.CANCELED.name()));
    }

    @Override
    public void pay(OrderVO order) {
        stateManager.setState(OrderStateContext.of(order), State.of(OrderState.PAID.name()));
    }

    @Override
    public void booked(OrderVO order) {
        stateManager.setState(OrderStateContext.of(order), State.of(OrderState.BOOKED.name()));
    }
}
