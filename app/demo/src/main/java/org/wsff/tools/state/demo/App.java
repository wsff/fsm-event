package org.wsff.tools.state.demo;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * App
 * @author ryan
 * @version Id: App.java, v 0.1 2022-03-28 18:52 ryan Exp $$
 */
public class App {


    public static void main(String[] args) throws IOException, InterruptedException {
        ApplicationContext context = new AnnotationConfigApplicationContext("org.wsff.tools.state");
        OrderService orderService = context.getBean(OrderService.class);
        System.out.println("+++++++++++++++++++++++++++++ORDER1+++++++++++++++++++++++++++++");
        // create order
        OrderVO order = orderService.create("ORDER" + System.currentTimeMillis());
        Thread.sleep(1000);

        // pay
        orderService.pay(order);
        Thread.sleep(1000);

        // booked
        orderService.booked(order);
        Thread.sleep(1000);

        System.out.println("+++++++++++++++++++++++++++++ORDER2+++++++++++++++++++++++++++++");
        // create order
        OrderVO order1 = orderService.create("ORDER1" + System.currentTimeMillis());

        // cancel
        orderService.cancel(order1);

    }
}
