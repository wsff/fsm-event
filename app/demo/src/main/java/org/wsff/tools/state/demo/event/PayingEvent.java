package org.wsff.tools.state.demo.event;

import org.wsff.tools.state.core.StateContext;

/**
 * PayingEvent
 * @author ryan
 * @version Id: PayingEvent.java, v 0.1 2022-03-28 18:00 ryan Exp $$
 */
public class PayingEvent extends BaseOrderEvent {

    private static final long serialVersionUID = 4926173229777598381L;

    public PayingEvent(Object source, StateContext context) {
        super(source, context);
    }
}
