package org.wsff.tools.state.demo;

import lombok.Data;

import java.io.Serializable;

/**
 * OrderVO
 * @author ryan
 * @version Id: OrderVO.java, v 0.1 2022-03-28 18:01 ryan Exp $$
 */
@Data
public class OrderVO implements Serializable {

    private static final long serialVersionUID = 1894819953059248001L;

    private String            id;

    private OrderState        state;

    public OrderVO(String id) {
        this.id = id;
        this.state = OrderState.DRAFT;
    }
}
