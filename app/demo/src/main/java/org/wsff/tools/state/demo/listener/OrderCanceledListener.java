package org.wsff.tools.state.demo.listener;

import org.springframework.stereotype.Service;
import org.wsff.tools.state.core.AbstractStateEventListener;
import org.wsff.tools.state.demo.OrderVO;
import org.wsff.tools.state.demo.event.CanceledEvent;
import org.wsff.tools.state.demo.impl.OrderStateContext;
import org.wsff.tools.state.event.BaseEvent;

/**
 * OrderCanceledListener
 * @author ryan
 * @version Id: OrderCanceledListener.java, v 0.1 2022-03-28 19:00 ryan Exp $$
 */
@Service
public class OrderCanceledListener extends AbstractStateEventListener<CanceledEvent> {

    /**
     * event handler
     *
     * @param event event
     */
    @Override
    public void onEvent(CanceledEvent event) {
        OrderVO order = ((OrderStateContext) event.getContext()).getOrder();
        System.out.println("Subscribe order " + order.getId() + " CanceledEvent");

    }

    /**
     * is support event
     *
     * @param event event
     * @return support
     */
    @Override
    public boolean isSupportEvent(BaseEvent event) {
        return event instanceof CanceledEvent;
    }
}
