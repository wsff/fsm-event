package org.wsff.tools.state.demo;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * OrderStateEnum
 * @author ryan
 * @version Id: OrderState.java, v 0.1 2022-03-28 17:49 ryan Exp $$
 */
@Getter
@AllArgsConstructor
public enum OrderState {

                        DRAFT("草稿"),

                        PAYING("待支付"),

                        CANCELED("已取消"),

                        PAID("已支付"),

                        BOOKING("预订中"),

                        BOOKED("已预订");

    /** desc */
    private final String                        desc;

    /** enums repo */
    public final static Map<String, OrderState> MAPPING = new HashMap<>();

    static {
        for (OrderState value : OrderState.values()) {
            MAPPING.put(value.name(), value);
        }
    }

    public static OrderState getByCode(String code) {
        return MAPPING.get(code);
    }
}
