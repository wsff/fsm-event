package org.wsff.tools.state.demo.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.wsff.tools.state.core.State;
import org.wsff.tools.state.core.StateContext;
import org.wsff.tools.state.core.StateTransferProvider;
import org.wsff.tools.state.core.spring.AbstractSpringStateManager;
import org.wsff.tools.state.demo.OrderState;
import org.wsff.tools.state.event.EventMulticaster;

/**
 * OrderStateManager
 * @author ryan
 * @version Id: OrderStateManager.java, v 0.1 2022-03-28 17:47 ryan Exp $$
 */
@Service
public class OrderStateManager extends AbstractSpringStateManager {

    @Resource(name = "orderEventMulticaster")
    private EventMulticaster eventMulticaster;

    @Resource(name = "orderStateTransferProvider")
    private StateTransferProvider stateTransferProvider;
    /**
     * update state
     *
     * @param context context
     * @param to to State
     * @return is success         
     */
    @Override
    protected boolean updateState(StateContext context, State to) {
        System.out.println("update db state success form-to: " + context.from().id() + "-" + to.id());
        ((OrderStateContext) context).getOrder().setState(OrderState.getByCode(to.id()));
        return true;
    }

    /**
     * init when the container is started
     */
    @Override
    public void init() {
        setEventMulticaster(eventMulticaster);
        setStateTransferProvider(stateTransferProvider);
        super.init();
    }
}
