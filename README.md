# fsm-event

#### Description
基于事件的状态机框架

#### core:
> 框架核心api
>
    核心API:
        1) StateManager:状态机状态流转入口类
        2) StateContext:状态机变更上下文
        3) StateTransfer:定义状态机转态流向，及状态流转时触发的事件
        4) State:状态
        5) StateEvent:状态机状态流转事件基类
        6) StateEventListener:态流转事件监听基类
        7) LifeCycle:框架初始化接口
        
#### event:
> 框架核心事件api，提供event bus能力
>
    核心API:
        1) EventMulticaster、SyncEventMulticaster(同步事件发布):提供事件发布能力
        2) BaseEventListener:事件订阅者基类
        3) BaseEvent:事件基类
        4) 两个实现 SimpleEventMulticaster(简介模式)、UnsharedExecutorEventMulticaster(线程资源不共享的模型)
        5) EventListenerSupport:EventListener管理支撑
#### fsm-event-spring:
> 提供spring实现
>
        1) 框架集成spring
#### demo:
> 一个示例
>
        1) 简单演示了fsm的编码实现及使用方法，入口方法{@link org.wsff.tools.state.demo.App}
